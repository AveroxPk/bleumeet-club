<?php
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.wowonder.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com   
// +------------------------------------------------------------------------+
// | WoWonder - The Ultimate Social Networking Platform
// | Copyright (c) 2016 WoWonder. All rights reserved.
// +------------------------------------------------------------------------+
require_once 'assets/import/PayPal/vendor/autoload.php';
require_once('functions_three.php');
if(Wo_main_company()){ // have to change at live
    $id     = $wo['config']['paypal_id'];
    $secret = $wo['config']['paypal_secret'];
    $mode   = $wo['config']['paypal_mode'];
}else{
    $id     = Wo_CompanyPaymentConfig()->paypal_id;
    $secret = Wo_CompanyPaymentConfig()->paypal_secret;
    $mode   = Wo_CompanyPaymentConfig()->paypal_mode;
}

$paypal = new \PayPal\Rest\ApiContext(
  new \PayPal\Auth\OAuthTokenCredential(
    $id,
    $secret
  )
);
$paypal->setConfig(
    array(
      'mode' => $mode
    )
);