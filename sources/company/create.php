<?php 
if ($wo['loggedin'] == true ) {
	  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
	  exit();
}

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'Register';
$wo['title']       = 'Register';
$wo['content']     = Wo_LoadPage('company/create');