<?php
if ($wo['loggedin'] == false) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}
if ($wo['config']['forum'] == 0) {
	header("Location: " . $wo['config']['site_url']);
    exit();
}

$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'forum';
$wo['active']      = 'forums';
if(Wo_Current_Company()->domain == "socialpublisher.club"){
    $wo['sections']    = Wo_GetForumSec(array("forums" => true,"limit" => 300));
}else{
    $company_id = Wo_Current_Company()->value;
    $wo['sections']    = Wo_GetForumSec(array("forums" => true,"company_id" => $company_id,"limit" => 300));
}
$wo['f-total']     = Wo_GetTotalForums();
$wo['title']       = 'Forum | ' . $wo['config']['siteTitle'];
$wo['content']     = Wo_LoadPage('forum/forum');