<?php
if ($wo['loggedin'] == false) {
  header("Location: " . Wo_SeoLink('index.php?link1=welcome'));
  exit();
}

if ($wo['config']['forum'] == 0) {
	header("Location: " . $wo['config']['site_url']);
    exit();
}
$wo['description'] = $wo['config']['siteDesc'];
$wo['keywords']    = $wo['config']['siteKeywords'];
$wo['page']        = 'forum';
$wo['active']      = 'members';
if(Wo_main_company()){
    $company_id  = Wo_Current_Company()->value;
    $wo['members']     = Wo_GetForumUsers(array('company_id' => $company_id));
}else{
    $company_id  = Wo_Current_Company()->value;
    $wo['members']   = Wo_GetForumUsers(array('company_id' => $company_id));
}
$wo['total_mbrs']  = Wo_GetTotalUsers();
$wo['char']        = null;
$wo['title']       = 'Forum Members';
$wo['content']     = Wo_LoadPage('forum/members');